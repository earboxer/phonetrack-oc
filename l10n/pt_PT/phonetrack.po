msgid ""
msgstr ""
"Project-Id-Version: phonetrack\n"
"Report-Msgid-Bugs-To: translations@owncloud.org\n"
"POT-Creation-Date: 2018-10-08 16:01+0200\n"
"PO-Revision-Date: 2018-10-08 14:16\n"
"Last-Translator: eneiluj <eneiluj@posteo.net>\n"
"Language-Team: Portuguese\n"
"Language: pt_PT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: phonetrack\n"
"X-Crowdin-Language: pt-PT\n"
"X-Crowdin-File: /master/l10n/templates/phonetrack.pot\n"

#: app.php:41
msgid "PhoneTrack"
msgstr ""

#: logcontroller.php:280 logcontroller.php:331
#, php-format
msgid "PhoneTrack proximity alert (%s and %s)"
msgstr ""

#: logcontroller.php:283
#, php-format
msgid "Device \"%s\" is now closer than %sm to \"%s\"."
msgstr ""

#: logcontroller.php:334
#, php-format
msgid "Device \"%s\" is now farther than %sm from \"%s\"."
msgstr ""

#: logcontroller.php:428 logcontroller.php:474
msgid "Geofencing alert"
msgstr ""

#: logcontroller.php:431
#, php-format
msgid "In session \"%s\", device \"%s\" entered geofencing zone \"%s\"."
msgstr ""

#: logcontroller.php:477
#, php-format
msgid "In session \"%s\", device \"%s\" exited geofencing zone \"%s\"."
msgstr ""

#: leaflet.js:5
msgid "left"
msgstr ""

#: leaflet.js:5
msgid "right"
msgstr ""

#: phonetrack.js:548 maincontent.php:69
msgid "Show lines"
msgstr ""

#: phonetrack.js:556
msgid "Hide lines"
msgstr ""

#: phonetrack.js:577
msgid "Activate automatic zoom"
msgstr ""

#: phonetrack.js:585
msgid "Disable automatic zoom"
msgstr ""

#: phonetrack.js:606
msgid "Show last point tooltip"
msgstr ""

#: phonetrack.js:614
msgid "Hide last point tooltip"
msgstr ""

#: phonetrack.js:635
msgid "Zoom on all devices"
msgstr ""

#: phonetrack.js:648
msgid "Click on the map to move the point, press ESC to cancel"
msgstr ""

#: phonetrack.js:888
msgid "Server name or server url should not be empty"
msgstr ""

#: phonetrack.js:891 phonetrack.js:900
msgid "Impossible to add tile server"
msgstr ""

#: phonetrack.js:897
msgid "A server with this name already exists"
msgstr ""

#: phonetrack.js:933 phonetrack.js:3647 maincontent.php:262 maincontent.php:306
#: maincontent.php:351 maincontent.php:400
msgid "Delete"
msgstr ""

#: phonetrack.js:966
msgid "Tile server \"{ts}\" has been added"
msgstr ""

#: phonetrack.js:969
msgid "Failed to add tile server \"{ts}\""
msgstr ""

#: phonetrack.js:973
msgid "Failed to contact server to add tile server"
msgstr ""

#: phonetrack.js:1007
msgid "Tile server \"{ts}\" has been deleted"
msgstr ""

#: phonetrack.js:1010
msgid "Failed to delete tile server \"{ts}\""
msgstr ""

#: phonetrack.js:1014
msgid "Failed to contact server to delete tile server"
msgstr ""

#: phonetrack.js:1158
msgid "Failed to contact server to restore options values"
msgstr ""

#: phonetrack.js:1161 phonetrack.js:1247 phonetrack.js:5444
msgid "Reload this page"
msgstr ""

#: phonetrack.js:1244
msgid "Failed to contact server to save options values"
msgstr ""

#: phonetrack.js:1267
msgid "Session name should not be empty"
msgstr ""

#: phonetrack.js:1284
msgid "Session name already used"
msgstr ""

#: phonetrack.js:1288
msgid "Failed to contact server to create session"
msgstr ""

#: phonetrack.js:1390
msgid "Watch this session"
msgstr ""

#: phonetrack.js:1396
msgid "shared by {u}"
msgstr ""

#: phonetrack.js:1401 phonetrack.js:2746
msgid "More actions"
msgstr ""

#: phonetrack.js:1405
msgid "Zoom on this session"
msgstr ""

#: phonetrack.js:1408
msgid "URL to share session"
msgstr ""

#: phonetrack.js:1412
msgid "URLs for logging apps"
msgstr ""

#: phonetrack.js:1416
msgid "Reserve device names"
msgstr ""

#: phonetrack.js:1425
msgid "Delete session"
msgstr ""

#: phonetrack.js:1427
msgid "Rename session"
msgstr ""

#: phonetrack.js:1430
msgid "Export to gpx"
msgstr ""

#: phonetrack.js:1435
msgid "Files are created in '{exdir}'"
msgstr ""

#: phonetrack.js:1436
msgid "Automatic export"
msgstr ""

#: phonetrack.js:1438
msgid "never"
msgstr ""

#: phonetrack.js:1439
msgid "daily"
msgstr ""

#: phonetrack.js:1440
msgid "weekly"
msgstr ""

#: phonetrack.js:1441
msgid "monthly"
msgstr ""

#: phonetrack.js:1446
msgid "Automatic purge is triggered daily and will delete points older than selected duration"
msgstr ""

#: phonetrack.js:1447
msgid "Automatic purge"
msgstr ""

#: phonetrack.js:1449
msgid "don't purge"
msgstr ""

#: phonetrack.js:1450
msgid "a day"
msgstr ""

#: phonetrack.js:1451
msgid "a week"
msgstr ""

#: phonetrack.js:1452
msgid "a month"
msgstr ""

#: phonetrack.js:1461
msgid "Name reservation is optional."
msgstr ""

#: phonetrack.js:1462
msgid "Name can be set directly in logging URL if it is not reserved."
msgstr ""

#: phonetrack.js:1463
msgid "To log with a reserved name, use its token in logging URL."
msgstr ""

#: phonetrack.js:1464
msgid "If a name is reserved, the only way to log with this name is with its token."
msgstr ""

#: phonetrack.js:1467
msgid "Reserve this device name"
msgstr ""

#: phonetrack.js:1469
msgid "Type reserved name and press 'Enter'"
msgstr ""

#: phonetrack.js:1483
msgid "Share with user"
msgstr ""

#: phonetrack.js:1485
msgid "Type user name and press 'Enter'"
msgstr ""

#: phonetrack.js:1490 phonetrack.js:4227
msgid "Shared with {u}"
msgstr ""

#: phonetrack.js:1496
msgid "A private session is not visible on public browser logging page"
msgstr ""

#: phonetrack.js:1498 phonetrack.js:5453
msgid "Make session public"
msgstr ""

#: phonetrack.js:1501 phonetrack.js:5448
msgid "Make session private"
msgstr ""

#: phonetrack.js:1506
msgid "Public watch URL"
msgstr ""

#: phonetrack.js:1508
msgid "API URL (JSON last positions)"
msgstr ""

#: phonetrack.js:1514
msgid "Current active filters will be applied on shared view"
msgstr ""

#: phonetrack.js:1516
msgid "Add public filtered share"
msgstr ""

#: phonetrack.js:1526
msgid "List of server URLs to configure logging apps."
msgstr ""

#: phonetrack.js:1527 maincontent.php:191
msgid "Replace 'yourname' with the desired device name or with the name reservation token"
msgstr ""

#: phonetrack.js:1529
msgid "Public browser logging URL"
msgstr ""

#: phonetrack.js:1531
msgid "OsmAnd URL"
msgstr ""

#: phonetrack.js:1535
msgid "GpsLogger GET and POST URL"
msgstr ""

#: phonetrack.js:1539
msgid "Owntracks (HTTP mode) URL"
msgstr ""

#: phonetrack.js:1543
msgid "Ulogger URL"
msgstr ""

#: phonetrack.js:1547
msgid "Traccar URL"
msgstr ""

#: phonetrack.js:1551
msgid "OpenGTS URL"
msgstr ""

#: phonetrack.js:1554
msgid "HTTP GET URL"
msgstr ""

#: phonetrack.js:1647
msgid "The session you want to delete does not exist"
msgstr ""

#: phonetrack.js:1650
msgid "Failed to delete session"
msgstr ""

#: phonetrack.js:1654
msgid "Failed to contact server to delete session"
msgstr ""

#: phonetrack.js:1674
msgid "Device '{d}' of session '{s}' has been deleted"
msgstr ""

#: phonetrack.js:1677
msgid "Failed to delete device '{d}' of session '{s}'"
msgstr ""

#: phonetrack.js:1681
msgid "Failed to contact server to delete device"
msgstr ""

#: phonetrack.js:1734
msgid "Impossible to rename session"
msgstr ""

#: phonetrack.js:1738
msgid "Failed to contact server to rename session"
msgstr ""

#: phonetrack.js:1786
msgid "Impossible to rename device"
msgstr ""

#: phonetrack.js:1790
msgid "Failed to contact server to rename device"
msgstr ""

#: phonetrack.js:1858
msgid "Impossible to set device alias for {n}"
msgstr ""

#: phonetrack.js:1862
msgid "Failed to contact server to set device alias"
msgstr ""

#: phonetrack.js:1928
msgid "Device already exists in target session"
msgstr ""

#: phonetrack.js:1931
msgid "Impossible to move device to another session"
msgstr ""

#: phonetrack.js:1935
msgid "Failed to contact server to move device"
msgstr ""

#: phonetrack.js:2004
msgid "Failed to contact server to get sessions"
msgstr ""

#: phonetrack.js:2412 phonetrack.js:2446
msgid "Stats of all points"
msgstr ""

#: phonetrack.js:2443
msgid "Stats of filtered points"
msgstr ""

#: phonetrack.js:2643
msgid "Device's color successfully changed"
msgstr ""

#: phonetrack.js:2646
msgid "Failed to save device's color"
msgstr ""

#: phonetrack.js:2650
msgid "Failed to contact server to change device's color"
msgstr ""

#: phonetrack.js:2739
msgid "Geo link to open position in other app/software"
msgstr ""

#: phonetrack.js:2741 phonetrack.js:2743 phonetrack.js:2745
msgid "Get driving direction to this device with {s}"
msgstr ""

#: phonetrack.js:2750
msgid "Delete this device"
msgstr ""

#: phonetrack.js:2752
msgid "Rename this device"
msgstr ""

#: phonetrack.js:2755
msgid "Set device alias"
msgstr ""

#: phonetrack.js:2758
msgid "Move to another session"
msgstr ""

#: phonetrack.js:2761 maincontent.php:48
msgid "Ok"
msgstr ""

#: phonetrack.js:2776
msgid "Device geofencing zones"
msgstr ""

#: phonetrack.js:2780
msgid "Zoom on geofencing area, then set values, then validate."
msgstr ""

#: phonetrack.js:2781 phonetrack.js:2833 phonetrack.js:4398 phonetrack.js:4489
msgid "Email notification"
msgstr ""

#: phonetrack.js:2783 phonetrack.js:4382
msgid "URL to request when entering"
msgstr ""

#: phonetrack.js:2784 phonetrack.js:2788 phonetrack.js:2836 phonetrack.js:2840
msgid "Use POST method"
msgstr ""

#: phonetrack.js:2787 phonetrack.js:4386
msgid "URL to request when leaving"
msgstr ""

#: phonetrack.js:2791
msgid "Geofencing zone coordinates"
msgstr ""

#: phonetrack.js:2791
msgid "leave blank to use current map bounds"
msgstr ""

#: phonetrack.js:2793
msgid "North"
msgstr ""

#: phonetrack.js:2795
msgid "South"
msgstr ""

#: phonetrack.js:2799
msgid "Set North/East corner by clicking on the map"
msgstr ""

#: phonetrack.js:2800
msgid "Set N/E"
msgstr ""

#: phonetrack.js:2802
msgid "Set South/West corner by clicking on the map"
msgstr ""

#: phonetrack.js:2803
msgid "Set S/W"
msgstr ""

#: phonetrack.js:2807
msgid "East"
msgstr ""

#: phonetrack.js:2809
msgid "West"
msgstr ""

#: phonetrack.js:2812
msgid "Fence name"
msgstr ""

#: phonetrack.js:2813
msgid "Use current map view as geofencing zone"
msgstr ""

#: phonetrack.js:2814
msgid "Add zone"
msgstr ""

#: phonetrack.js:2820
msgid "Device proximity notifications"
msgstr ""

#: phonetrack.js:2824
msgid "Select a session, a device name and a distance, set the notification settings, then validate."
msgstr ""

#: phonetrack.js:2825
msgid "You will be notified when distance between devices gets bigger than high limit or smaller than low limit."
msgstr ""

#: phonetrack.js:2826 maincontent.php:415 maincontent.php:433
msgid "Session"
msgstr ""

#: phonetrack.js:2828 maincontent.php:176 maincontent.php:420
#: maincontent.php:438
msgid "Device name"
msgstr ""

#: phonetrack.js:2829
msgid "Low distance limit"
msgstr ""

#: phonetrack.js:2831
msgid "High distance limit"
msgstr ""

#: phonetrack.js:2835 phonetrack.js:4487
msgid "URL to request when devices get close"
msgstr ""

#: phonetrack.js:2839 phonetrack.js:4488
msgid "URL to request when devices get far"
msgstr ""

#: phonetrack.js:2844
msgid "Add proximity notification"
msgstr ""

#: phonetrack.js:2855
msgid "Toggle detail/edition points"
msgstr ""

#: phonetrack.js:2863
msgid "Toggle lines"
msgstr ""

#: phonetrack.js:2871
msgid "Follow this device (autozoom)"
msgstr ""

#: phonetrack.js:2885 phonetrack.js:2894
msgid "Center map on device"
msgstr ""

#: phonetrack.js:3242
msgid "The point you want to edit does not exist or you're not allowed to edit it"
msgstr ""

#: phonetrack.js:3246
msgid "Failed to contact server to edit point"
msgstr ""

#: phonetrack.js:3364
msgid "The point you want to delete does not exist or you're not allowed to delete it"
msgstr ""

#: phonetrack.js:3368
msgid "Failed to contact server to delete point"
msgstr ""

#: phonetrack.js:3454 phonetrack.js:3488
msgid "Manually added"
msgstr ""

#: phonetrack.js:3472
msgid "Impossible to add this point"
msgstr ""

#: phonetrack.js:3476
msgid "Failed to contact server to add point"
msgstr ""

#: phonetrack.js:3603
msgid "Date"
msgstr ""

#: phonetrack.js:3606
msgid "Time"
msgstr ""

#: phonetrack.js:3611 phonetrack.js:3672
msgid "Altitude"
msgstr ""

#: phonetrack.js:3614 phonetrack.js:3677
msgid "Precision"
msgstr ""

#: phonetrack.js:3617 phonetrack.js:3684
msgid "Speed"
msgstr ""

#: phonetrack.js:3625 phonetrack.js:3689
msgid "Bearing"
msgstr ""

#: phonetrack.js:3628 phonetrack.js:3694
msgid "Satellites"
msgstr ""

#: phonetrack.js:3631 phonetrack.js:3699
msgid "Battery"
msgstr ""

#: phonetrack.js:3634 phonetrack.js:3703
msgid "User-agent"
msgstr ""

#: phonetrack.js:3637
msgid "lat : lng"
msgstr ""

#: phonetrack.js:3641
msgid "DMS coords"
msgstr ""

#: phonetrack.js:3646
msgid "Save"
msgstr ""

#: phonetrack.js:3648
msgid "Move"
msgstr ""

#: phonetrack.js:3649
msgid "Cancel"
msgstr ""

#: phonetrack.js:3850
msgid "Impossible to zoom, there is no point to zoom on for this session"
msgstr ""

#: phonetrack.js:3885
msgid "File extension must be '.gpx' to be imported"
msgstr ""

#: phonetrack.js:3903
msgid "Failed to create imported session"
msgstr ""

#: phonetrack.js:3907 phonetrack.js:3913
msgid "Failed to import session"
msgstr ""

#: phonetrack.js:3908
msgid "File is not readable"
msgstr ""

#: phonetrack.js:3914
msgid "File does not exist"
msgstr ""

#: phonetrack.js:3922
msgid "Failed to contact server to import session"
msgstr ""

#: phonetrack.js:3942
msgid "Session successfully exported in"
msgstr ""

#: phonetrack.js:3946
msgid "Failed to export session"
msgstr ""

#: phonetrack.js:3951
msgid "Failed to contact server to export session"
msgstr ""

#: phonetrack.js:3982
msgid "Failed to contact server to log position"
msgstr ""

#: phonetrack.js:4111
msgid "Impossible to zoom, there is no point to zoom on for this device"
msgstr ""

#: phonetrack.js:4150
msgid "'{n}' is already reserved"
msgstr ""

#: phonetrack.js:4153
msgid "Failed to reserve '{n}'"
msgstr ""

#: phonetrack.js:4156
msgid "Failed to contact server to reserve device name"
msgstr ""

#: phonetrack.js:4187 phonetrack.js:4191
msgid "Failed to delete reserved name"
msgstr ""

#: phonetrack.js:4188
msgid "This device does not exist"
msgstr ""

#: phonetrack.js:4192
msgid "This device name is not reserved, please reload this page"
msgstr ""

#: phonetrack.js:4195
msgid "Failed to contact server to delete reserved name"
msgstr ""

#: phonetrack.js:4215
msgid "User does not exist"
msgstr ""

#: phonetrack.js:4218
msgid "Failed to add user share"
msgstr ""

#: phonetrack.js:4221
msgid "Failed to contact server to add user share"
msgstr ""

#: phonetrack.js:4252
msgid "Failed to delete user share"
msgstr ""

#: phonetrack.js:4255
msgid "Failed to contact server to delete user share"
msgstr ""

#: phonetrack.js:4273 phonetrack.js:4297
msgid "Public share has been successfully modified"
msgstr ""

#: phonetrack.js:4276 phonetrack.js:4300
msgid "Failed to modify public share"
msgstr ""

#: phonetrack.js:4279 phonetrack.js:4303
msgid "Failed to contact server to modify public share"
msgstr ""

#: phonetrack.js:4321
msgid "Device name restriction has been successfully set"
msgstr ""

#: phonetrack.js:4324
msgid "Failed to set public share device name restriction"
msgstr ""

#: phonetrack.js:4327
msgid "Failed to contact server to set public share device name restriction"
msgstr ""

#: phonetrack.js:4360
msgid "Warning : User email and server admin email must be set to receive geofencing alerts."
msgstr ""

#: phonetrack.js:4364
msgid "Failed to add geofencing zone"
msgstr ""

#: phonetrack.js:4367
msgid "Failed to contact server to add geofencing zone"
msgstr ""

#: phonetrack.js:4389 phonetrack.js:4482
msgid "no"
msgstr ""

#: phonetrack.js:4391 phonetrack.js:4484
msgid "yes"
msgstr ""

#: phonetrack.js:4427
msgid "Failed to delete geofencing zone"
msgstr ""

#: phonetrack.js:4430
msgid "Failed to contact server to delete geofencing zone"
msgstr ""

#: phonetrack.js:4458
msgid "Warning : User email and server admin email must be set to receive proximity alerts."
msgstr ""

#: phonetrack.js:4462 phonetrack.js:4466
msgid "Failed to add proximity alert"
msgstr ""

#: phonetrack.js:4463
msgid "Device or session does not exist"
msgstr ""

#: phonetrack.js:4469
msgid "Failed to contact server to add proximity alert"
msgstr ""

#: phonetrack.js:4490
msgid "Low distance limit : {nbmeters}m"
msgstr ""

#: phonetrack.js:4491
msgid "High distance limit : {nbmeters}m"
msgstr ""

#: phonetrack.js:4519
msgid "Failed to delete proximity alert"
msgstr ""

#: phonetrack.js:4522
msgid "Failed to contact server to delete proximity alert"
msgstr ""

#: phonetrack.js:4541
msgid "Failed to add public share"
msgstr ""

#: phonetrack.js:4544
msgid "Failed to contact server to add public share"
msgstr ""

#: phonetrack.js:4563
msgid "Show this device only"
msgstr ""

#: phonetrack.js:4565
msgid "Show last positions only"
msgstr ""

#: phonetrack.js:4567
msgid "Simplify positions to nearest geofencing zone center"
msgstr ""

#: phonetrack.js:4586
msgid "No filters"
msgstr ""

#: phonetrack.js:4610
msgid "Failed to delete public share"
msgstr ""

#: phonetrack.js:4613
msgid "Failed to contact server to delete public share"
msgstr ""

#: phonetrack.js:4631
msgid "Failed to contact server to get user list"
msgstr ""

#: phonetrack.js:4644
msgid "device name"
msgstr ""

#: phonetrack.js:4645
msgid "distance (km)"
msgstr ""

#: phonetrack.js:4646
msgid "duration"
msgstr ""

#: phonetrack.js:4647
msgid "#points"
msgstr ""

#: phonetrack.js:4689
msgid "years"
msgstr ""

#: phonetrack.js:4692
msgid "days"
msgstr ""

#: phonetrack.js:4711
msgid "In OsmAnd, go to 'Plugins' in the main menu, then activate 'Trip recording' plugin and go to its settings."
msgstr ""

#: phonetrack.js:4712
msgid "Copy the URL below into the 'Online tracking web address' field."
msgstr ""

#: phonetrack.js:4716
msgid "In GpsLogger, go to 'Logging details' in the sidebar menu, then activate 'Log to custom URL'."
msgstr ""

#: phonetrack.js:4717
msgid "Copy the URL below into the 'URL' field."
msgstr ""

#: phonetrack.js:4725
msgid "In Ulogger, go to settings menu and copy the URL below into the 'Server URL' field."
msgstr ""

#: phonetrack.js:4726
msgid "Set 'User name' and 'Password' mandatory fields to any value as they will be ignored by PhoneTrack."
msgstr ""

#: phonetrack.js:4727
msgid "Activate 'Live synchronization'."
msgstr ""

#: phonetrack.js:4731
msgid "In Traccar client, copy the URL below into the 'server URL' field."
msgstr ""

#: phonetrack.js:4735
msgid "You can log with any other client with a simple HTTP request."
msgstr ""

#: phonetrack.js:4736
msgid "Make sure the logging system sets values for at least 'timestamp', 'lat' and 'lon' GET parameters."
msgstr ""

#: phonetrack.js:4739
msgid "Configure {loggingApp} for logging to session '{sessionName}'"
msgstr ""

#: phonetrack.js:4898
msgid "Are you sure you want to delete the session {session} ?"
msgstr ""

#: phonetrack.js:4901
msgid "Confirm session deletion"
msgstr ""

#: phonetrack.js:4959
msgid "Choose auto export target path"
msgstr ""

#: phonetrack.js:5121
msgid "Select storage location for '{fname}'"
msgstr ""

#: phonetrack.js:5379
msgid "Are you sure you want to delete the device {device} ?"
msgstr ""

#: phonetrack.js:5382
msgid "Confirm device deletion"
msgstr ""

#: phonetrack.js:5440
msgid "Failed to toggle session public status, session does not exist"
msgstr ""

#: phonetrack.js:5443
msgid "Failed to contact server to toggle session public status"
msgstr ""

#: phonetrack.js:5476
msgid "Failed to set session auto export value"
msgstr ""

#: phonetrack.js:5477 phonetrack.js:5504
msgid "session does not exist"
msgstr ""

#: phonetrack.js:5481
msgid "Failed to contact server to set session auto export value"
msgstr ""

#: phonetrack.js:5503
msgid "Failed to set session auto purge value"
msgstr ""

#: phonetrack.js:5508
msgid "Failed to contact server to set session auto purge value"
msgstr ""

#: phonetrack.js:5595
msgid "Import gpx session file"
msgstr ""

#: maincontent.php:4
msgid "Main tab"
msgstr ""

#: maincontent.php:5
msgid "Filters"
msgstr ""

#: maincontent.php:14
msgid "Stats"
msgstr ""

#: maincontent.php:15 maincontent.php:227
msgid "Settings and extra actions"
msgstr ""

#: maincontent.php:16 maincontent.php:619
msgid "About PhoneTrack"
msgstr ""

#: maincontent.php:36
msgid "Import session"
msgstr ""

#: maincontent.php:40
msgid "Create session"
msgstr ""

#: maincontent.php:44
msgid "Session name"
msgstr ""

#: maincontent.php:57
msgid "Options"
msgstr ""

#: maincontent.php:65
msgid "Auto zoom"
msgstr ""

#: maincontent.php:73
msgid "Show tooltips"
msgstr ""

#: maincontent.php:76
msgid "Refresh each (sec)"
msgstr ""

#: maincontent.php:79
msgid "Refresh"
msgstr ""

#: maincontent.php:81
msgid "An empty value means no limit"
msgstr ""

#: maincontent.php:83
msgid "Max number of points per device to load on refresh"
msgstr ""

#: maincontent.php:84
msgid "points"
msgstr ""

#: maincontent.php:86 maincontent.php:91
msgid "Cutting lines only affects map view and stats table"
msgstr ""

#: maincontent.php:88
msgid "Minimum distance to cut between two points"
msgstr ""

#: maincontent.php:89
msgid "meters"
msgstr ""

#: maincontent.php:93
msgid "Minimum time to cut between two points"
msgstr ""

#: maincontent.php:94
msgid "seconds"
msgstr ""

#: maincontent.php:98
msgid "Show direction arrows along lines"
msgstr ""

#: maincontent.php:102
msgid "Draw line with color gradient"
msgstr ""

#: maincontent.php:106
msgid "Show accuracy in tooltips"
msgstr ""

#: maincontent.php:110
msgid "Show speed in tooltips"
msgstr ""

#: maincontent.php:114
msgid "Show bearing in tooltips"
msgstr ""

#: maincontent.php:118
msgid "Show satellites in tooltips"
msgstr ""

#: maincontent.php:122
msgid "Show battery level in tooltips"
msgstr ""

#: maincontent.php:126
msgid "Show elevation in tooltips"
msgstr ""

#: maincontent.php:130
msgid "Show user-agent in tooltips"
msgstr ""

#: maincontent.php:135
msgid "Make points draggable in edition mode"
msgstr ""

#: maincontent.php:139
msgid "Show accuracy circle on hover"
msgstr ""

#: maincontent.php:142
msgid "Line width"
msgstr ""

#: maincontent.php:146
msgid "Point radius"
msgstr ""

#: maincontent.php:150
msgid "Points and lines opacity"
msgstr ""

#: maincontent.php:154
msgid "Theme"
msgstr ""

#: maincontent.php:156
msgid "bright"
msgstr ""

#: maincontent.php:157
msgid "pastel"
msgstr ""

#: maincontent.php:158
msgid "dark"
msgstr ""

#: maincontent.php:162
msgid "Auto export path"
msgstr ""

#: maincontent.php:167
msgid "Export one file per device"
msgstr ""

#: maincontent.php:169
msgid "reload page to make changes effective"
msgstr ""

#: maincontent.php:178
msgid "Log my position in this session"
msgstr ""

#: maincontent.php:183
msgid "Tracking sessions"
msgstr ""

#: maincontent.php:232
msgid "Custom tile servers"
msgstr ""

#: maincontent.php:235 maincontent.php:275 maincontent.php:318
#: maincontent.php:363
msgid "Server name"
msgstr ""

#: maincontent.php:236 maincontent.php:276 maincontent.php:319
#: maincontent.php:364
msgid "For example : my custom server"
msgstr ""

#: maincontent.php:237 maincontent.php:277 maincontent.php:320
#: maincontent.php:365
msgid "Server url"
msgstr ""

#: maincontent.php:238 maincontent.php:321
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:239 maincontent.php:279 maincontent.php:322
#: maincontent.php:367
msgid "Min zoom (1-20)"
msgstr ""

#: maincontent.php:241 maincontent.php:281 maincontent.php:324
#: maincontent.php:369
msgid "Max zoom (1-20)"
msgstr ""

#: maincontent.php:243 maincontent.php:287 maincontent.php:332
#: maincontent.php:381
msgid "Add"
msgstr ""

#: maincontent.php:246
msgid "Your tile servers"
msgstr ""

#: maincontent.php:272
msgid "Custom overlay tile servers"
msgstr ""

#: maincontent.php:278 maincontent.php:366
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:283 maincontent.php:371
msgid "Transparent"
msgstr ""

#: maincontent.php:285 maincontent.php:373
msgid "Opacity (0.0-1.0)"
msgstr ""

#: maincontent.php:290
msgid "Your overlay tile servers"
msgstr ""

#: maincontent.php:315
msgid "Custom WMS tile servers"
msgstr ""

#: maincontent.php:326 maincontent.php:375
msgid "Format"
msgstr ""

#: maincontent.php:328 maincontent.php:377
msgid "WMS version"
msgstr ""

#: maincontent.php:330 maincontent.php:379
msgid "Layers to display"
msgstr ""

#: maincontent.php:335
msgid "Your WMS tile servers"
msgstr ""

#: maincontent.php:360
msgid "Custom WMS overlay servers"
msgstr ""

#: maincontent.php:384
msgid "Your WMS overlay tile servers"
msgstr ""

#: maincontent.php:413
msgid "Manually add a point"
msgstr ""

#: maincontent.php:419 maincontent.php:437
msgid "Device"
msgstr ""

#: maincontent.php:421
msgid "Add a point"
msgstr ""

#: maincontent.php:422
msgid "Now, click on the map to add a point (if session is not activated, you won't see added point)"
msgstr ""

#: maincontent.php:423
msgid "Cancel add point"
msgstr ""

#: maincontent.php:428
msgid "Delete multiple points"
msgstr ""

#: maincontent.php:431
msgid "Choose a session, a device and adjust the filters. All displayed points for selected device will be deleted. An empty device name selects them all."
msgstr ""

#: maincontent.php:439
msgid "Delete points"
msgstr ""

#: maincontent.php:440
msgid "Delete only visible points"
msgstr ""

#: maincontent.php:447
msgid "Filter points"
msgstr ""

#: maincontent.php:452
msgid "Apply filters"
msgstr ""

#: maincontent.php:456
msgid "Begin date"
msgstr ""

#: maincontent.php:463 maincontent.php:481
msgid "today"
msgstr ""

#: maincontent.php:468
msgid "Begin time"
msgstr ""

#: maincontent.php:474
msgid "End date"
msgstr ""

#: maincontent.php:486
msgid "End time"
msgstr ""

#: maincontent.php:493
msgid "Min-- and Max--"
msgstr ""

#: maincontent.php:496
msgid "Min++ and Max++"
msgstr ""

#: maincontent.php:500
msgid "Last day:hour:min"
msgstr ""

#: maincontent.php:507
msgid "Minimum accuracy"
msgstr ""

#: maincontent.php:515
msgid "Maximum accuracy"
msgstr ""

#: maincontent.php:523
msgid "Minimum elevation"
msgstr ""

#: maincontent.php:531
msgid "Maximum elevation"
msgstr ""

#: maincontent.php:539
msgid "Minimum battery level"
msgstr ""

#: maincontent.php:547
msgid "Maximum battery level"
msgstr ""

#: maincontent.php:555
msgid "Minimum speed"
msgstr ""

#: maincontent.php:563
msgid "Maximum speed"
msgstr ""

#: maincontent.php:571
msgid "Minimum bearing"
msgstr ""

#: maincontent.php:579
msgid "Maximum bearing"
msgstr ""

#: maincontent.php:587
msgid "Minimum satellites"
msgstr ""

#: maincontent.php:595
msgid "Maximum satellites"
msgstr ""

#: maincontent.php:607
msgid "Statistics"
msgstr ""

#: maincontent.php:611
msgid "Show stats"
msgstr ""

#: maincontent.php:621
msgid "Shortcuts"
msgstr ""

#: maincontent.php:623
msgid "Toggle sidebar"
msgstr ""

#: maincontent.php:627
msgid "Documentation"
msgstr ""

#: maincontent.php:635
msgid "Source management"
msgstr ""

#: maincontent.php:649
msgid "Authors"
msgstr ""

